libio-digest-perl (0.11-5) unstable; urgency=medium

  * Team upload.
  * Add patch to fix warning with perl 5.40. (Closes: #1078112)
  * Update debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sat, 10 Aug 2024 18:22:37 +0200

libio-digest-perl (0.11-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libio-digest-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 07 Dec 2022 23:46:10 +0000

libio-digest-perl (0.11-3) unstable; urgency=medium

  * Team upload

  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Annotate test-only build dependencies with <!nocheck>.

 -- Damyan Ivanov <dmn@debian.org>  Mon, 27 Jun 2022 19:37:50 +0000

libio-digest-perl (0.11-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Add missing build dependency on libmodule-install-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 00:46:51 +0100

libio-digest-perl (0.11-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 16:36:47 +0100

libio-digest-perl (0.11-1) unstable; urgency=low

  * Import new upstream version 0.11
    - Fix "FTBFS with perl 5.18: old Module::Install". New upstream version
      updates included copy of Module::Install. (Closes: #709870)
  * Take over for the Debian Perl Group on maintainer's request
    (https://lists.debian.org/debian-perl/2013/08/msg00050.html)
  * debian/control: Added: Vcs-Git field (source stanza); Vcs-Browser
    field (source stanza). Changed: Maintainer set to Debian Perl Group
    <pkg-perl-maintainers@lists.alioth.debian.org> (was: Bastian Blank
    <waldi@debian.org>).
  * Add debian/watch.
  * Update debian/copyright file information
    Update format to copyright-format 1.0 as released together with Debian
    policy 3.9.3.
    Add copyright stanza for included Module::Install copy.
    Update copyright for debian/* packaging files
  * Convert package to '3.0 (quilt)' source package format
  * Adjust short and long description for module
    For short description try to match the 'is a' Perl module scheme and
    mention the module name in the long description.
  * Add myself to Uploaders
  * Bump Debhelper compat level to 8.
    Adjust versioned Build-Depends for debhelper to (>= 8).
  * Make Build-Depends-Indep on perl unversioned
  * Make (build) dependency for libperlio-via-dynamic-perl unversioned.
    The version required for libperlio-via-dynamic-perl is already satisfied
    in oldstable.
  * Add Homepage field for debian/control file
  * Reduce debian/rules to a tiny 3-line dh-style makefile
  * Bump Standards-Version to 3.9.4
  * Wrap and sort fields in debian/control file

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 13 Aug 2013 14:47:59 +0200

libio-digest-perl (0.10-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS with Perl 5.10.  Closes: #468030.

 -- Mark Hymers <mhy@debian.org>  Sat, 05 Apr 2008 14:30:30 +0100

libio-digest-perl (0.10-1) unstable; urgency=low

  * Initial Release.

 -- Bastian Blank <waldi@debian.org>  Fri, 24 Sep 2004 12:45:41 +0200
